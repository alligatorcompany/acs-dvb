#!/bin/bash
kubectl create secret generic dvb-secrets \
    --from-literal=authenticator_password=${PWD} \
    --from-literal=core_dbadmin_password=${PWD} \
    --from-literal=scheduler_password=${PWD} \
    --from-literal=systems_password_private_keypassword=start123 \
    --from-file=secrets/systems_password_private_key \
    --from-file=secrets/systems_password_public_key \
    --from-file=secrets/datavault_builder_license \
    --type=Opaque --dry-run=client -o yaml